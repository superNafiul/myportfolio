/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var navItems;



function initNav() {

    var dataFile = "js/nav_bar_item.json";
    loadData(dataFile);


}

function loadData(jsonFile) {


    $.getJSON(jsonFile, function (json) {

        loadJSONData(json);
        //console.log("entered");
        addNavItems();
    });






}

function loadJSONData(data) {

    //Navigation Items
    navItems = new Array();
    for (var i = 0; i < data.nav.length; i++) {


        navItems[i] = data.nav[i];
    }



}

function addNavItems(){
    
    
    
    
    /**
     * {"tag-name":"", "class": "" , "id": "", "link": "", "title":""}
     * 
     */
    
    var token = $("#navbar");
    
    for(var i=0; i< navItems.length; i++){
        
        token.append(
                
                "<a class=\"" + navItems[i].class +"\" "
                + " href=\"" + navItems[i].link + "\" "
                
                + ">" + navItems[i].title + "</a>"
                
                );
    }
    
    
}


