/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * @author NAFIUL AZIM
 */

var intro;
var education;
var skills;
var courses;
var project;
var experience;
var activity;

var layout;


function initResume() {

    var dataFile = "js/resume.json";
    loadData(dataFile);


}

function loadData(jsonFile) {


    $.getJSON(jsonFile, function (json) {

        loadJSONData(json);
        //console.log("entered");
        buildLayout();
        addIntro();
        addEducation();
        addSkills();
        addCourses();
        addProject();
        addExperience();
        addActivity();
    });






}

function loadJSONData(data) {

    //intro
    intro = new Array();
    for (var i = 0; i < data.intro.length; i++) {


        intro[i] = data.intro[i];
    }



    //education
    education = new Array();
    for (var i = 0; i < data.education.length; i++) {

        education[i] = data.education[i];
    }


    //Skill
    skills = new Array();
    for (var i = 0; i < data.skill.length; i++) {

        skills[i] = data.skill[i];
    }

    //courses
    courses = new Array();
    for (var i = 0; i < data.courses.length; i++) {

        courses[i] = data.courses[i];
    }

    //project
    project = new Array();
    for (var i = 0; i < data.project.length; i++) {

        project[i] = data.project[i];
    }

    //experience
    experience = new Array();
    for (var i = 0; i < data.experience.length; i++) {

        experience[i] = data.experience[i];

    }

    //activities
    activity = new Array();
    for (var i = 0; i < data.activities.length; i++) {

        activity[i] = data.activities[i];
    }


    // addIntro();

}


function buildLayout() {

    var lay = $("#resumeLayout");


    lay.append("<section id = \"intro\"></section>");
    lay.append("<section id = \"education\"></section>");
    lay.append("<section id = \"skill\"></section>");
    lay.append("<section id = \"course\"></section>");
    lay.append("<section id = \"project\"></section>");
    lay.append("<section id = \"experience\"></section>");
    lay.append("<section id = \"activity\"></section>");


}
;

function addIntro() {

    var key = $("#intro");

    key.append("<h1>Name & Contact Information</h1>");

    for (var i = 0; i < intro.length; i++) {

        key.append("<div>"

                + intro[i].name + "<br>"
                + intro[i].address + "<br>"
                + intro[i].email + "<br><br>"
                + "</div>"

                );
    }


}

function addEducation() {


    var key = $("#education");

    key.append("<h1>Education</h1>");

    for (var i = 0; i < education.length; i++) {

        key.append("<div>"

                + "<b>"+ education[i].school +"</b>" + "<br>"
                + education[i].year + "," + education[i].major + "<br>"
                + education[i].graduation + "<br><br>"
                + "<br>"
                + "</div>"
                );
    }

}

function addSkills() {

    var key = $("#skill");

    key.append("<h1>Skills</h1>");

    for (var i = 0; i < skills.length; i++) {

        key.append("<div>"

                + skills[i].type + ":" + skills[i].skillset
                + "<br><br>"
                + "</div>"
                );
    }
}

function addCourses() {

    var key = $("#course");

    key.append("<h1>Relevant Coursework</h1>");

    for (var i = 0; i < courses.length; i++) {

        key.append("<div>"

                + courses[i].title + "<br><br>"
                + "</div>"

                );
    }
}

function addProject() {

    var key = $("#project");

    key.append("<h1>Projects</h1>");

    for (var i = 0; i < project.length; i++) {

        key.append("<div>"

                + "<b>" + project[i].title + "</b>" + "<br>"
                + project[i].type + ", " + project[i].date + "<br>"
                + project[i].description + "<br><br>"
                + "</div>"

                );
    }
}

function addExperience() {

    var key = $("#experience");

    key.append("<h1>Experience</h1>");

    for (var i = 0; i < experience.length; i++) {

        key.append("<div><b>"

                + experience[i].position + "," + experience[i].institution + "</b><br>"
                + experience[i].description + "<br><br>"
                + "</div>"

                );
    }

}

function addActivity() {

    var key = $("#activity");

    key.append("<h1>Activities</h1>");

    for (var i = 0; i < activity.length; i++) {

        key.append("<div><b>"

                + activity[i].org + "</b><br>"
                + activity[i].joined + "<br>"
                + activity[i].activity + "<br><br>"
                + "</div>"

                );
    }
}